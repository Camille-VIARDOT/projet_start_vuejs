import Vue from 'vue'
import Vuex from 'vuex'
import App from './App.vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import VueRouter from 'vue-router'
import ListTodo from "./components/ListTodo.vue";
import addForm from "./components/addForm.vue";
import store from './store'

Vue.use(Vuex)
Vue.use(VueRouter)
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.config.productionTip = false



const routes = [
  { path: '/new', component: addForm },
  { path: '/list', component: ListTodo, props: { whatToDisplay: "all" } },
  { path: '/done', component: ListTodo, props: { whatToDisplay: "done" } },
  { path: '/todo', component: ListTodo, props: { whatToDisplay: "todo" } },
]

const router = new VueRouter({
  routes // raccourci pour `routes: routes`
})

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')

