import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({

    state: {
        todo: [{}]
    },

    getters: {
        trieTodo: state => (whatToDisplay) => {
            console.log(whatToDisplay);
            if (whatToDisplay == 'todo') {
                let array = []
                for (let i = 0; i < state.todo.length; i++) {
                    if (state.todo[i].todo == true) {
                        array.push(state.todo[i])
                    }
                }
                return array

            } else if (whatToDisplay == 'done') {
                let array = []
                for (let i = 0; i < state.todo.length; i++) {
                    if (state.todo[i].todo == false) {
                        array.push(state.todo[i])
                    }
                }
                return array

            } else {
                return state.todo
            }
        },

    },

    mutations: {
        RECUP_API(state, myTodo) {
            state.todo = myTodo
        },

        CHANGE_STATUS(state, status) {
            let index = state.todo.findIndex(elem => elem.id == status)
            state.todo[index].todo = !state.todo[index].todo
        }

    },

    actions: {
        recupApi(context, myTodo) {
            context.commit('RECUP_API', myTodo)
        },

        changeStatus(context, status) {
            context.commit('CHANGE_STATUS', status)
        }
    }
})

