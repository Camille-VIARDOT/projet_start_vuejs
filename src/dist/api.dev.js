"use strict";

var mongoose = require('mongoose');

var express = require('express');

var app = express();
var port = 2000;
mongoose.connect('mongodb://localhost/TodoList_Vuejs', {
  useNewUrlParser: true,
  useUnifiedTopology: true
});
app.use(express.json());
app.use(express.urlencoded({
  extended: true
})); // Schéma de la Todo

var todoSchema = new mongoose.Schema({
  name: String,
  id: Number,
  createdAt: String,
  todo: Boolean
}); //Schéma pour la collection 

var Todo = mongoose.model('TodoList', todoSchema); //Compile le schéma
//Route de la Todo

app.post('/todo', function _callee(req, res) {
  var newTodo;
  return regeneratorRuntime.async(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          newTodo = new Todo({
            name: req.body.name,
            id: req.body.id,
            createdAt: req.body.createdAt,
            todo: req.body.todo
          });
          newTodo.save(function (err, newTodo) {
            if (err) return console.log(err);
            newTodo.name;
            res.json('Vous avez ajouté : ' + newTodo);
          });

        case 2:
        case "end":
          return _context.stop();
      }
    }
  });
}); //Connection au server express

app.listen(port, function () {
  console.log('Connection au serveur ok !');
});