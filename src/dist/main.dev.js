"use strict";

var _vue = _interopRequireDefault(require("vue"));

var _App = _interopRequireDefault(require("./App.vue"));

var _bootstrapVue = require("bootstrap-vue");

var _vueRouter = _interopRequireDefault(require("vue-router"));

var _ListTodo = _interopRequireDefault(require("./components/ListTodo.vue"));

var _addForm = _interopRequireDefault(require("./components/addForm.vue"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

_vue["default"].use(_vueRouter["default"]);

_vue["default"].use(_bootstrapVue.BootstrapVue);

_vue["default"].use(_bootstrapVue.IconsPlugin);

_vue["default"].config.productionTip = false;
var Foo = {
  template: '<div>foo</div>'
};
var Bar = {
  template: '<div>bar</div>'
};
var routes = [{
  path: '/new',
  component: _addForm["default"]
}, {
  path: '/list',
  component: Foo,
  props: {
    whatToDisplay: "all"
  }
}, {
  path: '/done',
  component: Bar,
  props: {
    whatToDisplay: "done"
  }
}, {
  path: '/todo',
  component: _ListTodo["default"],
  props: {
    whatToDisplay: "todo"
  }
}];
var router = new _vueRouter["default"]({
  routes: routes // raccourci pour `routes: routes`

});
new _vue["default"]({
  router: router,
  render: function render(h) {
    return h(_App["default"]);
  }
}).$mount('#app');