const mongoose = require('mongoose');
const express = require('express');
let app = express();
let port = 2000;
let cors = require('cors');


mongoose.connect('mongodb://localhost/TodoList_Vuejs', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
});

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({
    extended: true
}));

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    next();
});


// Schéma de la Todo
const todoSchema = new mongoose.Schema({
    name: String,
    id: Number,
    createdAt: String,
    todo: Boolean
}) //Schéma de la collection 
const Todo = mongoose.model('TodoList', todoSchema); //Compile le schéma

//Route de la Todo

app.get('/todo', async (req, res) => {
    try {
        Todo.find((err, todo) => {
            res.json(todo);
        })
    } catch (error) {
        console.log(error)
    }
})

app.get('/todo/:id', async (req, res) => {
    try {
        Todo.find((err, todo) => {
            for (let i = 0; i < todo.length; i++) {
                if (req.params.id == todo[i].id) {
                    res.json(todo[i])
                }
            }
            res.json(peperoni);
        })
    } catch (error) {
        console.log(error)
    }
})

app.post('/todo', async (req, res) => {
    try {
        const newTodo = new Todo({
            name: req.body.name,
            id: req.body.id,
            createdAt: req.body.createdAt,
            todo: req.body.todo
        });
        newTodo.save()

    } catch (error) {
        console.log(error);
    }
})


app.put('/todo/:id', async (req, res) => {
    try {
        let doc = await Todo.findOneAndUpdate({ id: req.params.id }, { todo: req.body.todo });
        doc.save()
    } catch (error) {
        console.log(error)
    }
})




//Connection au server express
app.listen(port, () => {
    console.log('Connection au serveur ok !');
})